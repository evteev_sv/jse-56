package com.nlmk.evteev.controller;

import com.nlmk.evteev.entity.Chat;
import com.nlmk.evteev.entity.Message;
import com.nlmk.evteev.entity.User;
import com.nlmk.evteev.repository.ChatRepository;
import com.nlmk.evteev.repository.MessageRepository;
import com.nlmk.evteev.repository.UserRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class AppController {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss").withZone(ZoneId.systemDefault());
    private static Scanner scanner = new Scanner(System.in);
    private UserRepository userRepository = new UserRepository();
    private MessageRepository messageRepository = new MessageRepository();
    private ChatRepository chatRepository = new ChatRepository();
    private String cmd = "";
    private User currentUser = null;
    private Chat currentChat = null;
    private LocalDateTime lastDate;
    private Timer timer;
    private TimerTask timerTask;

    public void initController() {
        sayWelcome();
        doLogin();
        doEnterChat();
        System.out.println("If you want to exit from app type EXIT...");
        while (!"EXIT".equalsIgnoreCase(cmd)) {
            printAndSaveMessage();
        }
    }

    private void sayWelcome() {
        System.out.println("Welcome to chat!");
    }

    private void doLogin() {
        System.out.println("Enter your login:");
        cmd = scanner.nextLine();
        if (!cmd.isEmpty()) {
            Optional<User> res = userRepository.findByLogin(cmd.toUpperCase());
            res.ifPresentOrElse(user -> currentUser = user, () -> currentUser = createUser(cmd));
        }
    }

    private void doEnterChat() {
        System.out.println("Avialable chats: ");
        List<Chat> chats = chatRepository.findAll();
        chats.forEach(chat -> System.out.println("Chat name: ".concat(chat.getChatName())));
        System.out.println("Enter chat name:");
        cmd = scanner.nextLine();
        if (cmd == null || cmd.isEmpty()) {
            doEnterChat();
        } else {
            List<Chat> filteredChat = chats.stream()
                    .filter(chat -> chat.getChatName().equalsIgnoreCase(cmd))
                    .collect(Collectors.toList());
            if (filteredChat.isEmpty()) {
                Chat chat = new Chat();
                chat.setChatName(cmd);
                currentChat = chatRepository.save(chat);
            } else {
                currentChat = filteredChat.get(0);
            }
            lastDate = LocalDateTime.now();
            createSchedule(100, 1000);
        }
    }

    private User createUser(String login) {
        if (login != null && !login.isEmpty()) {
            User user = new User();
            user.setUserLogin(login.toUpperCase());
            user.setDateCreate(LocalDateTime.now());
            user = userRepository.save(user);
            return user;
        } else {
            return null;
        }
    }

    private void createSchedule(long delay, long period) {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                checkNewMessage(currentChat, currentUser, lastDate);
            }
        };
        timer.schedule(timerTask, delay, period);
    }

    private void checkNewMessage(Chat chat, User user, LocalDateTime date) {
        List<Message> msgList = messageRepository.findAllByChatAndNotOwnedByUser(chat, user, date);
        msgList.forEach(message -> System.out.println(String.format("[From time %s User %s say] >> %s",
                message.getMsgDate().format(formatter),
                message.getSpeaker().getUserLogin(),
                message.getMsg())));
        lastDate = LocalDateTime.now();
    }

    private void printAndSaveMessage() {
        cmd = scanner.nextLine();
        if (cmd != null && !cmd.isBlank() && !cmd.isEmpty() && !"EXIT".equalsIgnoreCase(cmd)) {
            Message message = new Message();
            message.setMsg(cmd);
            message.setMsgDate(LocalDateTime.now());
            message.setChat(currentChat);
            message.setSpeaker(currentUser);
            currentChat.getMessages().add(message);
            messageRepository.save(message);
        }
    }

}
