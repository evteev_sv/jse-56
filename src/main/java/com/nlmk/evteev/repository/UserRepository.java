package com.nlmk.evteev.repository;

import com.nlmk.evteev.entity.User;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Log4j2
public class UserRepository extends EntityRepository {

    public List<User> findAll() {
        Transaction tx = null;
        try {
            Session session = sessionFactory.openSession();
            tx = session.beginTransaction();
            List<User> result = session.createQuery("SELECT u from User u", User.class).getResultList();
            tx.commit();
            session.close();
            return result;
        } catch (Exception ee) {
            log.error(ee.getMessage());
            return Collections.emptyList();
        }
    }

    public Optional<User> findByLogin(String login) {
        Transaction tx = null;
        Optional<User> result = Optional.empty();
        try {
            Session session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Query<User> q = session.createQuery("SELECT u from User u where u.userLogin = :userLogin", User.class);
            q.setParameter("userLogin", login.toUpperCase());
            if (q.getSingleResult() == null) {
                result = Optional.empty();
            } else {
                result = Optional.of(q.getSingleResult());
            }
            tx.commit();
            session.close();
        } catch (Exception ee) {
            log.error(ee.getMessage());
            result = Optional.empty();
        }
        return result;
    }

}
