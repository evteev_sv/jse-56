package com.nlmk.evteev.repository;

import com.nlmk.evteev.entity.Chat;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Log4j2
public class ChatRepository extends EntityRepository {

    public List<Chat> findAll() {
        Transaction tx = null;
        try {
            Session session = sessionFactory.openSession();
            tx = session.beginTransaction();
            List<Chat> result = session.createQuery("Select c from Chat c", Chat.class).getResultList();
            tx.commit();
            session.close();
            return result;
        } catch (Exception ee) {
            log.error(ee.getMessage());
            return Collections.emptyList();
        }
    }

    public Optional<Chat> findByName(String chatName) {
        Transaction tx = null;
        Optional<Chat> result = Optional.empty();
        try {
            Session session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Query<Chat> q = session.createQuery("Select c from Chat c where c.chatName = :chatName", Chat.class);
            q.setParameter("chatName", chatName);
            result = q.uniqueResultOptional();
            tx.commit();
            session.close();
        } catch (Exception ee) {
            log.error(ee.getMessage());
        }
        return result;
    }
}
