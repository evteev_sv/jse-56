package com.nlmk.evteev.repository;

import com.nlmk.evteev.entity.Chat;
import com.nlmk.evteev.entity.Message;
import com.nlmk.evteev.entity.User;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.TemporalType;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
public class MessageRepository extends EntityRepository {

    /**
     * Возвращаем все сообщения
     *
     * @return {@link List}
     */
    public List<Message> findAll() {
        Transaction tx = null;
        try {
            Session session = sessionFactory.openSession();
            tx = session.beginTransaction();
            List<Message> result = session.createQuery("Select c from Message c", Message.class).getResultList();
            tx.commit();
            session.close();
            return result;
        } catch (Exception ee) {
            log.error(ee.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Возвращаем все сообщения пользователя независимо от чата
     *
     * @param user пользователь {@link User}
     * @return список сообщений {@link List}
     */
    public List<Message> findAllByUser(User user) {
        List<Message> result = findAll().stream()
                .filter(message -> message.getSpeaker().equals(user))
                .collect(Collectors.toList());
        return result;
    }

    /**
     * Возвращаем все сообщения чата, независимо от пользователя
     *
     * @param chat чат {@link Chat}
     * @return список сообщений {@link List}
     */
    public List<Message> findAllByChat(Chat chat) {
        List<Message> result = findAll().stream()
                .filter(message -> message.getChat().equals(chat))
                .collect(Collectors.toList());
        return result;
    }

    /**
     * Возвращаем все сообщения в чате, которые отправлял не указанный пользователь
     *
     * @param chat чат {@link Chat}
     * @param user пользователь, чьи сообщения игнорируются {@link User}
     * @return список сообщений {@link List}
     */
    public List<Message> findAllByChatAndNotOwnedByUser(Chat chat, User user, LocalDateTime lastDate) {
        Transaction tx = null;
        try {
            Session session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Query<Message> q = session.createQuery("Select c from Message c " +
                            " where c.chat.chatId = :chatId and c.speaker.userId <> :userId " +
                            " and c.msgDate >= :msgDate",
                    Message.class);
            q.setParameter("chatId", chat.getChatId());
            q.setParameter("userId", user.getUserId());
            q.setParameter("msgDate", lastDate, TemporalType.TIMESTAMP);
            List<Message> result = q.getResultList();
            tx.commit();
            session.close();
            return result;
        } catch (Exception ee) {
            log.error(ee.getMessage());
            return Collections.emptyList();
        }
    }

}
