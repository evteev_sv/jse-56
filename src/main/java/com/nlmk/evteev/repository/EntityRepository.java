package com.nlmk.evteev.repository;

import com.nlmk.evteev.config.HibernateConfig;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

@Log4j2
public abstract class EntityRepository {

    public SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public <T> T save(T entity) {
        Transaction tx = null;
        try {
            Session session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.save(entity);
            tx.commit();
            session.close();
        } catch (Exception ee) {
            if (tx != null) {
                tx.rollback();
            }
            log.error(ee.getMessage());
        }
        return entity;
    }

}
