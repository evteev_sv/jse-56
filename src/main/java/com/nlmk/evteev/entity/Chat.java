package com.nlmk.evteev.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "chat")
public class Chat implements Serializable {

    private static final long serialVersionUID = 2813707702031194784L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "chat_id", unique = true)
    private Long chatId;

    @Column(name = "chat_name", unique = true, length = 50, nullable = false)
    private String chatName;

//    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user_id")
//    @JoinTable(
//            name = "user_chat",
//            joinColumns = {@JoinColumn(name = "chat_id", nullable = false)},
//            inverseJoinColumns = {@JoinColumn(name = "user_id", nullable = false)}
//    )
//    private List<User> users = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "chat")
    private List<Message> messages = new ArrayList<>();

}
