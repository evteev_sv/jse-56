package com.nlmk.evteev.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_user")
public class User implements Serializable {

    private static final long serialVersionUID = 5084326748641153540L;

    @Id
    @Column(name = "user_id", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @Column(name = "user_login", length = 15, unique = true)
    private String userLogin;

    @Column(name = "date_create", columnDefinition = "TIMESTAMP")
    private LocalDateTime dateCreate;

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "speaker")
//    private List<Message> messages;
//
//    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "users")
//    private List<Chat> chats;

}
