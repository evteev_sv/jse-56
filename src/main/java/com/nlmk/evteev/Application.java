package com.nlmk.evteev;

import com.nlmk.evteev.controller.AppController;

public class Application {


    public static void main(String[] args) {
        AppController appController = new AppController();
        appController.initController();
    }
}
